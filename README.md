# KeePass Plugin to mount VeraCrypt Volume File | Partition | Disk  #

It's a fork of [KeePassTrueCryptMount](https://bitbucket.org/schalpat/keepasstruecryptmount/overview)

It's working as well a the previous plugin for TrueCrypt, and add a few mount options new to VeraCrypt.

## Have a look at the [Wiki](https://bitbucket.org/fbourqui/keepassveracryptmount/wiki/Home) ##

### current version: 1.0.7.0 ###
Issue/Request implemented
 
 fix parameter k missing a space before it, when using a keyfile

### Older release notes ###

### current version: 1.0.6.0 ###
Issue/Request implemented
 
 fix looking for executable with file dialog in Linux
 
 Fix typo in global config panel

### version: 1.0.5.0 ###
Issue/Request implemented
 
 #8 : fixed Ubutu PPA: Incompatible with 2.36, tested on latest ubuntu and mint, not on other distrib:

    - ubuntu 17.10 with keepass2 from ubuntu universe repo.

    - mint 18.2 with ppa 

    Disk letter are remapped to VeraCrypt slot number.

    Options removable and beep disabled as they do not exist on linux

#### version: 1.0.4.0 ####

Issue/Request implemented
 
 #9 : fixed Keepass 2.3.6 freezes after veracrypt automount, added thread sleep after autotype
 
 #10: Fix Auto-mount volume without ask password, removed silent and backgroud flag (background is mandatory to get password window for either autotype or ask password)
 
 fixed using shortcut CTRL+m if "m* key stay pressed a little after CTRL was released, it mess with autotype

#### version: 1.0.3.0 ####
Issue/Request implemented
 
 #7: Fix typo in VeraCrypt option menu title
 
#### version: 1.0.2.0 ####
Issue/Request implemented
 
 #4: Make it so we can use Volume ID to mount a drive/partition
 
 fix bug when removing PIM from an entry

#### version: 1.0.1.0 ####

 #3: Proposal : Add optional setting of Windows Explorer volume label to mount panel

 Reorganized TabIndex of mount pannel
 
#### version: 1.0.0.4 ####

 added tooltip info on "Hide dialogue next time" mount flag (will not work if global plugin option "Show VeraCrypt Mount menu item on each KeePass entry" is activated)

#### version: 1.0.0.3 ####

 #1: Request: Include PIM value setting

 #2: Request: Add selection of PKCS-5 hash algo to speed up opening of volume (VeraCrypt doesn't need to go through all hashes algo) 
 
#### version: 1.0.0.2 ####
Issue/Request implemented from KeePassTrueCryptMount:

 #16: Support relative paths and keepass placeholders

 #25: Request: Mount with timestamp option

 #26: Request: Veracrypt version
