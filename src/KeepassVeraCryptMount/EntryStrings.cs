﻿/*
  Plugin for KeePass Password Safe - The Open-Source Password Manager
  KeePassVeraCryptMount
  Copyright (C) 2015-2017 Frédéric Bourqui

  fork of:
  KeePassTrueCryptMount
  Copyright (C) 2010-2013 Patrick Schaller

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

namespace KeePassVeraCryptMount
{
    internal static class EntryStrings
    {
        public const string Enabled = "veracrypt.enabled";

        public const string DriveLetter = "veracrypt.drive";

        public const string Volume = "veracrypt.container";

        public const string KeyFiles = "veracrypt.keyfiles";

        public const string Readonly = "veracrypt.option.readonly";

        public const string Removable = "veracrypt.option.removable";

        public const string Silent = "veracrypt.option.silent";

        public const string Background = "veracrypt.option.quit";

        public const string Beep = "veracrypt.option.beep";

        public const string Explorer = "veracrypt.option.explorer";

        public const string MountWithoutDialog = "veracrypt.mount.without.dialog";

        public const string AskForPassword = "veracrypt.option.askpassword";

        public const string TrueCryptMode = "veracrypt.option.truecryptmode";

        public const string TimeStamp = "veracrypt.option.timestamp";

        public const string PIM = "veracrypt.option.pim";

        public const string PKCS5 = "veracrypt.option.pkcs5";

        public const string Label = "veracrypt.option.label";
    }
}
