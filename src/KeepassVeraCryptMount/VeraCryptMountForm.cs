﻿/*
  Plugin for KeePass Password Safe - The Open-Source Password Manager
  KeePassVeraCryptMount
  Copyright (C) 2015-2017 Frédéric Bourqui

  fork of:
  KeePassTrueCryptMount
  Copyright (C) 2010-2013 Patrick Schaller

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

namespace KeePassVeraCryptMount
{
    using System;

    using System.Drawing;
    using System.Linq;
    using System.Text;
    using System.Windows.Forms;
    using KeePass.UI;
    using KeePassLib;
    using KeePassLib.Security;

    public partial class VeraCryptMountForm : Form
    {
        public VeraCryptMountForm()
        {
            InitializeComponent();

            this.driveComboBox.Items.Add(new AutoDriveLetterComboBoxItem());
            this.driveComboBox.Items.AddRange(LogicalDriveLetters.FreeDriveLetters.ToArray());
        }

        public string ContainerVolume
        {
            get { return this.volumeTextBox.Text; }
        }

        public string KeyFiles
        {
            get { return this.keyFilesTextBox.Text; }
        }

        public string PIM
        {
            get { return this.PIMtextBox.Text; }
        }

        public string PKCS5
        {
            get { return this.PKCS5comboBox.SelectedItem as string; }
        }

        public string DriveLetter
        {
            get
            {
                return this.driveComboBox.SelectedItem is AutoDriveLetterComboBoxItem 
                                         ? string.Empty 
                                         : this.driveComboBox.SelectedItem as string;
            }
        }

        public string Label
        {
            get { return this.labelTextBox.Text; }
        }

        public bool OptionReadOnly
        {
            get { return this.readonlyCheckBox.Checked; }
            private set { this.readonlyCheckBox.Checked = value; }
        }

        public bool OptionRemovable
        {
            get { return this.removableCheckBox.Checked; }
            private set { this.removableCheckBox.Checked = value; }
        }

        public bool OptionTimeStamp
        {
            get { return this.timestampCheckBox.Checked; }
            private set { this.timestampCheckBox.Checked = value; }
        }

        public bool OptionTruecrypt
        {
            get { return this.truecryptCheckBox.Checked; }
            private set { this.truecryptCheckBox.Checked = value; }
        }

        public bool HideForEntry
        {
            get { return this.hideDialogCheckBox.Checked; }
        }

        public bool ChangesApplied { get; set; }

        public VeraCryptMountForm ReadFrom(PwEntry entry)
        {
            // container
            var containerString = entry.Strings.GetSafe(EntryStrings.Volume);
            this.volumeTextBox.Text = containerString != null ? containerString.ReadString() : string.Empty;

            // KeyFiles
            var KeyFilesString = entry.Strings.GetSafe(EntryStrings.KeyFiles);
            this.keyFilesTextBox.Text = KeyFilesString != null ? KeyFilesString.ReadString() : string.Empty;

            // drive
            this.driveComboBox.SelectedIndex = 0;

            var driveString = entry.Strings.GetSafe(EntryStrings.DriveLetter);

            if (driveString != null)
            {
                var checkedValue = driveString.ReadString().EnsureIsDrive();

                if (!string.IsNullOrEmpty(checkedValue))
                {
                    this.driveComboBox.SelectedItem = checkedValue;
                }
            }

            // label
            if (Tools.Unix)
            {
                this.labelTextBox.Visible = false;
                this.labelVolume.Visible = false;
            }
            else
            {
                var labelString = entry.Strings.GetSafe(EntryStrings.Label);
                this.labelTextBox.Text = labelString != null ? labelString.ReadString() : string.Empty;
            }

            // readonly
            this.OptionReadOnly = false;
            var readonlyString = entry.Strings.GetSafe(EntryStrings.Readonly);
            if (!readonlyString.IsEmpty)
            {
                bool readOnly;
                bool.TryParse(readonlyString.ReadString(), out readOnly);
                this.OptionReadOnly = readOnly;
            }

            // timestamp
            this.OptionTimeStamp = false;
            var timestampString = entry.Strings.GetSafe(EntryStrings.TimeStamp);
            if (!timestampString.IsEmpty)
            {
                bool timeStamp;
                bool.TryParse(timestampString.ReadString(), out timeStamp);
                this.OptionTimeStamp = timeStamp;
            }

            // removable
            if (Tools.Unix) this.removableCheckBox.Enabled = false;
            this.removableCheckBox.Checked = false;
            var removableString = entry.Strings.GetSafe(EntryStrings.Removable);
            if (!removableString.IsEmpty)
            {
                bool removable;
                bool.TryParse(removableString.ReadString(), out removable);
                this.removableCheckBox.Checked = removable;
            }

            // beep
            if (Tools.Unix) this.beepCheckBox.Enabled = false;
            this.beepCheckBox.Checked = false;
            var beepString = entry.Strings.GetSafe(EntryStrings.Beep);
            if (!beepString.IsEmpty)
            {
                bool beep;
                bool.TryParse(beepString.ReadString(), out beep);
                this.beepCheckBox.Checked = beep;
            }

            // explorer
            this.explorerCheckBox.Checked = false;
            var explorerString = entry.Strings.GetSafe(EntryStrings.Explorer);
            if (!explorerString.IsEmpty)
            {
                bool explorer;
                bool.TryParse(explorerString.ReadString(), out explorer);
                this.explorerCheckBox.Checked = explorer;
            }

            // hide dialog
            this.hideDialogCheckBox.Checked = false;
            var hideDialogString = entry.Strings.GetSafe(EntryStrings.MountWithoutDialog);
            if (!hideDialogString.IsEmpty)
            {
                bool hideDialog;
                bool.TryParse(hideDialogString.ReadString(), out hideDialog);
                this.hideDialogCheckBox.Checked = hideDialog;
            }

            // ask for password/Auto-Type
            // kept parameter name askPassword, but renamed form option to Auto-type (reversing the meaning of the checkbox)
            this.autoTypeCheckBox.Checked = true;
            var askPasswordString = entry.Strings.GetSafe(EntryStrings.AskForPassword);
            if (!askPasswordString.IsEmpty)
            {
                bool askPassword;
                bool.TryParse(askPasswordString.ReadString(), out askPassword);
                this.autoTypeCheckBox.Checked = !askPassword;
            }
            
            // truecrypt
            this.truecryptCheckBox.Checked = false;
            var truecryptString = entry.Strings.GetSafe(EntryStrings.TrueCryptMode);
            if (!truecryptString.IsEmpty)
            {
                bool truecrypt;
                bool.TryParse(truecryptString.ReadString(), out truecrypt);
                this.truecryptCheckBox.Checked = truecrypt;
            }

            // PIM
            var PIMString = entry.Strings.GetSafe(EntryStrings.PIM);
            if (!PIMString.IsEmpty)
            {
                String PIMStr = PIMString.ReadString();
                int PIMInt;
                if (Int32.TryParse(PIMStr, out PIMInt))
                    if (PIMInt > 0 && PIMInt < 10000000)
                    {
                        this.PIMtextBox.Text = PIMStr;
                        this.UsePIMcheckBox.Checked = true;
                    }
            }

            // PKCS5
            var PKCS5String = entry.Strings.GetSafe(EntryStrings.PKCS5);
            this.PKCS5comboBox.SelectedItem = (PKCS5String != null && !PKCS5String.IsEmpty) ? PKCS5String.ReadString() : "Autodetection";

            this.btnApply.Enabled = false;

            return this;
        }

        public VeraCryptMountForm WriteTo(PwEntry entry)
        {
            entry.Strings.Set(EntryStrings.Volume, new ProtectedString(false, this.ContainerVolume));
            entry.Strings.Set(EntryStrings.KeyFiles, new ProtectedString(false, this.KeyFiles));
            entry.Strings.Set(EntryStrings.Label, new ProtectedString(false, this.Label));
            entry.Strings.Set(EntryStrings.DriveLetter, new ProtectedString(false, this.DriveLetter));
            entry.Strings.Set(EntryStrings.Enabled, new ProtectedString(false, true.ToString()));
            entry.Strings.Set(EntryStrings.MountWithoutDialog, new ProtectedString(false, this.HideForEntry.ToString()));
            entry.Strings.Remove(EntryStrings.Background);
            entry.Strings.Remove(EntryStrings.Silent);
            entry.Strings.Set(EntryStrings.TrueCryptMode, new ProtectedString(false, this.truecryptCheckBox.Checked.ToString()));
            entry.Strings.Set(EntryStrings.Removable, new ProtectedString(false, this.removableCheckBox.Checked.ToString()));
            entry.Strings.Set(EntryStrings.Readonly, new ProtectedString(false, this.readonlyCheckBox.Checked.ToString()));
            entry.Strings.Set(EntryStrings.TimeStamp, new ProtectedString(false, this.timestampCheckBox.Checked.ToString()));
            entry.Strings.Set(EntryStrings.Beep, new ProtectedString(false, this.beepCheckBox.Checked.ToString()));
            entry.Strings.Set(EntryStrings.Explorer, new ProtectedString(false, this.explorerCheckBox.Checked.ToString()));
            entry.Strings.Set(EntryStrings.AskForPassword, new ProtectedString(false, (!this.autoTypeCheckBox.Checked).ToString()));
            if (!this.truecryptCheckBox.Checked)
            {
                if (this.PKCS5comboBox.SelectedIndex > 0)
                    entry.Strings.Set(EntryStrings.PKCS5, new ProtectedString(false, this.PKCS5));
                else entry.Strings.Remove(EntryStrings.PKCS5);

                int PIMInt;
                if (Int32.TryParse(PIM, out PIMInt))
                {
                    if (UsePIMcheckBox.Checked && PIMInt > 0)
                    {
                        entry.Strings.Set(EntryStrings.PIM, new ProtectedString(true, this.PIM));
                    }
                    else entry.Strings.Remove(EntryStrings.PIM);
                }
                else entry.Strings.Remove(EntryStrings.PIM);
            }
            entry.AutoType.DefaultSequence = "{PASSWORD}{ENTER}";
            entry.AutoType.ObfuscationOptions = KeePassLib.Collections.AutoTypeObfuscationOptions.UseClipboard;

            return this;
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            this.Icon = Icon.FromHandle(Resources.VeraCryptNormal.GetHicon());

            this.bannerPanel.BackgroundImage = BannerFactory.CreateBanner(
                this.bannerPanel.Width,
                this.bannerPanel.Height,
                BannerStyle.Default,
                Resources.B48_VeraCrypt,
                LanguageTexts.BannerPluginTitleText,
                LanguageTexts.BannerMountFormDescriptionText);

            this.btnApply.Click += this.OnApplyButtonClicked;

            GlobalWindowManager.AddWindow(this);
        }

        protected override void OnFormClosed(FormClosedEventArgs e)
        {
            this.btnApply.Click -= this.OnApplyButtonClicked;

            GlobalWindowManager.RemoveWindow(this);

            base.OnFormClosed(e);
        }

        private void OnAnyOptionChanged(object sender, EventArgs e)
        {
            this.btnApply.Enabled = true;
        }

        private void OnApplyButtonClicked(object sender, EventArgs e)
        {
            this.ChangesApplied = true;
            this.btnApply.Enabled = false;
        }

        private void OnFileOpenDialogButtonClicked(object sender, EventArgs e)
        {
            var dialog = new OpenFileDialog();
            if (sender.Equals(button2))
            {
                dialog.Multiselect = true;
                if (dialog.ShowDialog(this) == DialogResult.OK && dialog.CheckFileExists)
                {
                    var keyFiles = new StringBuilder();
                    foreach (string i in dialog.FileNames)
                    {
                        keyFiles.Append(i + ";");
                    }
                    this.keyFilesTextBox.Text = keyFiles.ToString();
                }
            }else if(sender.Equals(button1))
            {
                dialog.Multiselect = false;
            
                if (dialog.ShowDialog(this) == DialogResult.OK && dialog.CheckFileExists)
                {
                    this.volumeTextBox.Text = dialog.FileName.ToString();
                }
            }
        }

        private void OnReloadDrivesButtonClicked(object sender, EventArgs e)
        {
            var selectedItem = this.driveComboBox.SelectedItem;

            this.driveComboBox.Items.Clear();
            this.driveComboBox.Items.Add(new AutoDriveLetterComboBoxItem());
            this.driveComboBox.Items.AddRange(LogicalDriveLetters.FreeDriveLetters.ToArray());

            if (selectedItem == null || selectedItem is AutoDriveLetterComboBoxItem)
            {
                this.driveComboBox.SelectedIndex = 0;
            }
            else
            {
                this.driveComboBox.SelectedItem = selectedItem;
            }
        }

        private void PIMtextBox_TextChanged(object sender, EventArgs e)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(PIM, "[^0-9]"))
            {
                MessageBox.Show(LanguageTexts.VCPIMOnlyNumber, LanguageTexts.VCPIMError, MessageBoxButtons.OK, MessageBoxIcon.Error);
                PIMtextBox.Text = "";
            }
            this.btnApply.Enabled = true;
        }


        private void UsePIM_CheckedChanged(object sender, EventArgs e)
        {
            this.PIMtextBox.Visible = !PIMtextBox.Visible;
            this.btnApply.Enabled = true;
        }

        private void truecrypt_CheckedChanged(object sender, EventArgs e)
        {
            this.PKCS5label.Enabled = !PKCS5label.Enabled;
            this.PKCS5comboBox.Enabled = !PKCS5comboBox.Enabled;
            this.UsePIMcheckBox.Enabled = !UsePIMcheckBox.Enabled;
            this.PIMtextBox.Enabled = !PIMtextBox.Enabled;
            this.btnApply.Enabled = true;

        }
    }
}
