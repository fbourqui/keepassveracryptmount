﻿/*
  Plugin for KeePass Password Safe - The Open-Source Password Manager
  KeePassVeraCryptMount
  Copyright (C) 2015-2017 Frédéric Bourqui

  fork of:
  KeePassTrueCryptMount
  Copyright (C) 2010-2013 Patrick Schaller

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

namespace KeePassVeraCryptMount
{
    internal static class CustomConfigKeys
    {
        public const string VeraCryptExecuteable = "veracrypt.exe.location";

        public const string VeraCryptAutoTypeWaitTimeout = "veracrypt.autotype.wait.timeout";

        //Not used anymore
        public const string VeraCryptMenuItemAlwaysVisible = "veracrypt.menuitem.always.visible";
    }
}
