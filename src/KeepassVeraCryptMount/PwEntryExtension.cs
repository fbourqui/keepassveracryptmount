﻿/*
  Plugin for KeePass Password Safe - The Open-Source Password Manager
  KeePassVeraCryptMount
  Copyright (C) 2015-2017 Frédéric Bourqui

  fork of:
  KeePassTrueCryptMount
  Copyright (C) 2010-2013 Patrick Schaller

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

namespace KeePassVeraCryptMount
{
    using System;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Windows.Forms;

    using KeePassLib;
    using KeePass.Util;
	using KeePass.Util.Spr;
    using System.IO;

    internal static class PwEntryExtension
    {
        
        public static string ToVeraCryptArguments(this PwEntry entry)
        {
            char prefix = Tools.Unix ? '-' : '/';

            var arguments = new StringBuilder();
            var mount = new StringBuilder();

            // background is mandatory to get passord entry for autotype or in ask password mode in windows
            // bacground in linux is not mandatory for autotype to work but it will lauch tray tool
            if (Tools.Unix)
            {
                arguments.Append("--background-task ");
            }
            else
            {
                arguments.Append("/q background ");
            }

            // beep not exist in unix
            {
                var beepString = entry.Strings.GetSafe(EntryStrings.Beep).ReadString();
                bool beep;
                bool.TryParse(beepString, out beep);
                if (beep & !Tools.Unix)
                {
                    arguments.Append("/beep ");
                }
            }

            // readonly
            {
                var readonlyString = entry.Strings.GetSafe(EntryStrings.Readonly).ReadString();
                bool readOnly;
                bool.TryParse(readonlyString, out readOnly);
                if (readOnly)
                {
                    mount.Append(prefix + "m ro ");
                }
            }

            // timestamp
            {
                var timestampString = entry.Strings.GetSafe(EntryStrings.TimeStamp).ReadString();
                bool timeStamp;
                bool.TryParse(timestampString, out timeStamp);
                if (timeStamp)
                {
                    mount.Append(prefix + "m ts ");
                }
            }

            // removable
            {
                var removableString = entry.Strings.GetSafe(EntryStrings.Removable).ReadString();
                bool removable;
                bool.TryParse(removableString, out removable);
                if (removable & !Tools.Unix)
                {
                    mount.Append("/m rm ");
                }
            }

			// label not exist in unix
			{
                var labelString = entry.Strings.GetSafe(EntryStrings.Label).ReadString();
                if (!string.IsNullOrEmpty(labelString) & !Tools.Unix)
                {
                    mount.Append("/m label=" + labelString.QuoteIfNecessary() + " ");
                }
            }
            if (Tools.Unix & mount.ToString().Length>6) {
                mount.Clear();
                mount.Append("-m ro,ts ");
            }
            arguments.Append(mount);

            // explore
            {
                var exploreString = entry.Strings.GetSafe(EntryStrings.Explorer).ReadString();
                bool explore;
                bool.TryParse(exploreString, out explore);
                if (explore)
                {
                    if (Tools.Unix) arguments.Append(prefix);
                    arguments.Append(prefix + "explore ");
                }
            }

            // truecrypt mode
            {
                var truecryptString = entry.Strings.GetSafe(EntryStrings.TrueCryptMode).ReadString();
                bool truecrypt;
                bool.TryParse(truecryptString, out truecrypt);
                if (truecrypt)
                {
                    arguments.Append(prefix + "tc ");
                }
                else
                {
                    // PIM
                    var PIMString = entry.Strings.GetSafe(EntryStrings.PIM).ReadString();
                    int PIMInt;
                    if (Int32.TryParse(PIMString, out PIMInt))
                        if (PIMInt > 0 && PIMInt < 10000000)
                        {
                            if (Tools.Unix) arguments.Append(prefix);
                            arguments.Append(prefix + "pim " + PIMString + " ");
                        }
                    // PKCS5
                    var PKCS5String = entry.Strings.GetSafe(EntryStrings.PKCS5);
                    if (!PKCS5String.IsEmpty)
                    {
                        if (Tools.Unix) arguments.Append(prefix);
                        // remove HMAC- from string
                        arguments.Append(prefix + "hash " + PKCS5String.ReadString().Substring(5) + " ");
                    }
                }
            }

            // drive letter
            {
                var driveString = entry.Strings.GetSafe(EntryStrings.DriveLetter).ReadString();
                if (!string.IsNullOrEmpty(driveString))
                {
                    if (Tools.Unix)
                    {
                        arguments.Append("--slot " + Tools.CapLetterToSlot(driveString[0]));
                    }
                    else
                    {
                        arguments.Append("/letter " + driveString[0]);
                    }
                    arguments.Append(" ");
                }
            }

            // container volume
            {
                var volumeString = entry.Strings.GetSafe(EntryStrings.Volume).ReadString();
                if (!Tools.Unix)
                {
                    arguments.Append("/v ");
                }
                if (string.IsNullOrEmpty(volumeString))
                {
                    MessageBox.Show(LanguageTexts.VCNoVolumeSpecifiedError, LanguageTexts.VCMountError, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return "";
                }

                if (!Regex.IsMatch(volumeString, @"^ID:.*"))
                {
                    if (!Path.IsPathRooted(volumeString))
                    {
                        volumeString = SprEngine.Compile(volumeString, new SprContext(entry, null, SprCompileFlags.Paths));
                        volumeString = Path.GetFullPath(volumeString);
                    }
                }

                if (!File.Exists(volumeString))
                {
                    if (!Tools.Unix)
                    {
                        //Make sure it's a Volume File, not a drive or partition
                        if (Regex.IsMatch(volumeString, @"^(([a-zA-Z]:|\\\\\w[ \w\.]*)(\\\w[ \w\.]*|\\%[ \w\.]+%+)+|%[ \w\.]+%(\\\w[ \w\.]*|\\%[ \w\.]+%+)*)"))
                        {
                            MessageBox.Show(LanguageTexts.VCVolumeNotFound + volumeString, LanguageTexts.VCMountError, MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return "";
                        }
                    }
                }
                
                arguments.Append(volumeString.QuoteIfNecessary());
            }
			
            // Keyfiles
            {
                var keyFilesString = entry.Strings.GetSafe(EntryStrings.KeyFiles).ReadString();

                if (!string.IsNullOrEmpty(keyFilesString))
                {
                    string[] keyFiles = keyFilesString.Split(';');
                    for (int i = 0; i < keyFiles.Length; i++)
                    {
                        var keyFile = keyFiles[i];

                        if (string.IsNullOrEmpty(keyFile))
                        {
                            continue;
                        }

                        if (!Path.IsPathRooted(keyFile))
                        {
                            keyFile = SprEngine.Compile(keyFile, new SprContext(entry, null, SprCompileFlags.Paths | SprCompileFlags.EnvVars));
                        }

                        arguments.Append(" " + prefix + "k ");
                        arguments.Append(keyFile.GetFullPath().QuoteIfNecessary());
                    }
                }
               
            }
            return arguments.ToString();
        }

        public static bool ShouldHideMountDialog(this PwEntry entry)
        {
            if (entry == null)
            {
                return true;
            }

            bool hideDialog;
            var hideDialogString = entry.Strings.GetSafe(EntryStrings.MountWithoutDialog);

            if (!hideDialogString.IsEmpty)
            {
                bool.TryParse(hideDialogString.ReadString(), out hideDialog);
            }
            else
            {
                hideDialog = false;
            }

            return hideDialog;
        }

        public static bool HasMountSettings(this PwEntry entry)
        {
            if (entry == null)
            {
                return false;
            }

            return bool.TrueString.Equals(entry.Strings.GetSafe(EntryStrings.Enabled).ReadString());
        }

        public static void PerformPasswordAutotype(this PwEntry entry,PwDatabase database)
        {
            var askForPasswordString = entry.Strings.GetSafe(EntryStrings.AskForPassword).ReadString();
            bool askForPassword;
            bool.TryParse(askForPasswordString, out askForPassword);

            if (!askForPassword)
            {
                AutoType.PerformIntoCurrentWindow(entry,database);
            }
        }

        /// <summary>
        /// Quote unquote the specified file path if it contains spaces.
        /// </summary>
        /// <param name="filePath">The file path.</param>
        /// <returns>A quoted file path if it contains spaces.</returns>
        private static string QuoteIfNecessary(this string filePath)
        {
            if (filePath.Contains(" ") && !filePath.StartsWith("\"") && !filePath.EndsWith("\""))
            {
                return string.Format("\"{0}\"", filePath);
            }

            return filePath;
        }

        /// <summary>
        /// Returns the absolute path for the specified path string.
        /// </summary>
        /// <param name="filePath">The file or directory for which to obtain absolute path information.</param>
        /// <returns>The fully qualified location of <see cref="filePath"/>, such as "C:\MyFile.txt".</returns>
        private static string GetFullPath(this string filePath)
        {
            try
            {
                return Path.GetFullPath(filePath);
            }
            catch (NotSupportedException)
            {
            }
            catch (PathTooLongException)
            {
            }
            catch (ArgumentNullException)
            {
            }
            catch (ArgumentException)
            {
            }

            return filePath;
        }
    }
}
