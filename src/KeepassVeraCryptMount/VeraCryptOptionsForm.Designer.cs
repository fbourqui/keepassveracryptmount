﻿namespace KeePassVeraCryptMount
{
    partial class VeraCryptOptionsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.txtVeraCryptExecutable = new System.Windows.Forms.TextBox();
            this.ofdVeraCryptExecutable = new System.Windows.Forms.OpenFileDialog();
            this.lblSeparator = new System.Windows.Forms.Label();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnOk = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.tipRegistryButton = new System.Windows.Forms.ToolTip(this.components);
            this.btnRegistryResolve = new System.Windows.Forms.Button();
            this.picBanner = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnOpenFileDialog = new System.Windows.Forms.Button();
            this.llblDonate = new System.Windows.Forms.LinkLabel();
            this.llableHelp = new System.Windows.Forms.LinkLabel();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.numericAutoTypeTimeout = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.picBanner)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericAutoTypeTimeout)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 77);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(137, 13);
            this.label1.TabIndex = 13;
            this.label1.Text = "VeraCrypt executable path";
            // 
            // txtVeraCryptExecutable
            // 
            this.txtVeraCryptExecutable.BackColor = System.Drawing.Color.Coral;
            this.txtVeraCryptExecutable.Location = new System.Drawing.Point(6, 93);
            this.txtVeraCryptExecutable.Name = "txtVeraCryptExecutable";
            this.txtVeraCryptExecutable.Size = new System.Drawing.Size(441, 21);
            this.txtVeraCryptExecutable.TabIndex = 12;
            this.txtVeraCryptExecutable.TextChanged += new System.EventHandler(this.OnVeraCryptPathTextChanged);
            // 
            // ofdVeraCryptExecutable
            // 
            this.ofdVeraCryptExecutable.FileName = "VeraCrypt.exe";
            this.ofdVeraCryptExecutable.Filter = "VeraCrypt Executable|VeraCrypt.exe";
            this.ofdVeraCryptExecutable.RestoreDirectory = true;
            // 
            // lblSeparator
            // 
            this.lblSeparator.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblSeparator.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblSeparator.Location = new System.Drawing.Point(0, 254);
            this.lblSeparator.Name = "lblSeparator";
            this.lblSeparator.Size = new System.Drawing.Size(520, 11);
            this.lblSeparator.TabIndex = 15;
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Location = new System.Drawing.Point(454, 260);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 1;
            this.btnClose.Text = "&Close";
            this.btnClose.UseVisualStyleBackColor = true;
            // 
            // btnOk
            // 
            this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOk.Location = new System.Drawing.Point(372, 260);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 0;
            this.btnOk.Text = "&OK";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.OnOkButtonClicked);
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(7, 125);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(300, 67);
            this.label2.TabIndex = 17;
            this.label2.Text = "Using Ctrl+M will always launch VeraCrypt Mount volume pannel, for new or existin" +
    "g entry.";
            // 
            // tipRegistryButton
            // 
            this.tipRegistryButton.IsBalloon = true;
            this.tipRegistryButton.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.tipRegistryButton.ToolTipTitle = "Auto find";
            // 
            // btnRegistryResolve
            // 
            this.btnRegistryResolve.Image = global::KeePassVeraCryptMount.Resources.B16x16_System_Registry;
            this.btnRegistryResolve.Location = new System.Drawing.Point(494, 91);
            this.btnRegistryResolve.Name = "btnRegistryResolve";
            this.btnRegistryResolve.Size = new System.Drawing.Size(35, 23);
            this.btnRegistryResolve.TabIndex = 15;
            this.tipRegistryButton.SetToolTip(this.btnRegistryResolve, "Resolves the VeraCrypt executable from the Registry");
            this.btnRegistryResolve.UseVisualStyleBackColor = true;
            this.btnRegistryResolve.Click += new System.EventHandler(this.OnRegistryResolveButtonClicked);
            // 
            // picBanner
            // 
            this.picBanner.Location = new System.Drawing.Point(0, 0);
            this.picBanner.Name = "picBanner";
            this.picBanner.Size = new System.Drawing.Size(540, 70);
            this.picBanner.TabIndex = 19;
            this.picBanner.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.BackgroundImage = global::KeePassVeraCryptMount.Resources.MenuItemSample;
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.panel1.Location = new System.Drawing.Point(313, 125);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(216, 67);
            this.panel1.TabIndex = 18;
            // 
            // btnOpenFileDialog
            // 
            this.btnOpenFileDialog.Image = global::KeePassVeraCryptMount.Resources.B16x16_Folder_Yellow_Open;
            this.btnOpenFileDialog.Location = new System.Drawing.Point(453, 91);
            this.btnOpenFileDialog.Name = "btnOpenFileDialog";
            this.btnOpenFileDialog.Size = new System.Drawing.Size(35, 23);
            this.btnOpenFileDialog.TabIndex = 14;
            this.btnOpenFileDialog.UseVisualStyleBackColor = true;
            this.btnOpenFileDialog.Click += new System.EventHandler(this.OnButtonOpenFileDialogClick);
            // 
            // llblDonate
            // 
            this.llblDonate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.llblDonate.AutoSize = true;
            this.llblDonate.Location = new System.Drawing.Point(3, 265);
            this.llblDonate.Name = "llblDonate";
            this.llblDonate.Size = new System.Drawing.Size(42, 13);
            this.llblDonate.TabIndex = 24;
            this.llblDonate.TabStop = true;
            this.llblDonate.Text = "Donate";
            this.llblDonate.VisitedLinkColor = System.Drawing.Color.Blue;
            this.llblDonate.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.OnDonateLabelLinkClicked);
            // 
            // llableHelp
            // 
            this.llableHelp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.llableHelp.AutoSize = true;
            this.llableHelp.Location = new System.Drawing.Point(80, 265);
            this.llableHelp.Name = "llableHelp";
            this.llableHelp.Size = new System.Drawing.Size(28, 13);
            this.llableHelp.TabIndex = 25;
            this.llableHelp.TabStop = true;
            this.llableHelp.Text = "Help";
            this.llableHelp.VisitedLinkColor = System.Drawing.Color.Blue;
            this.llableHelp.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.OnHelpLabelLinkClicked);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 200);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(145, 13);
            this.label4.TabIndex = 20;
            this.label4.Text = "AutoType Execution Timeout";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(69, 219);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(20, 13);
            this.label5.TabIndex = 22;
            this.label5.Text = "ms";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(99, 217);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(310, 26);
            this.label6.TabIndex = 23;
            this.label6.Text = "On slower computers or containers on external network drives,\r\nit is recommed to " +
    "increase this timeout.";
            // 
            // numericAutoTypeTimeout
            // 
            this.numericAutoTypeTimeout.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numericAutoTypeTimeout.Location = new System.Drawing.Point(6, 217);
            this.numericAutoTypeTimeout.Maximum = new decimal(new int[] {
            60000,
            0,
            0,
            0});
            this.numericAutoTypeTimeout.Minimum = new decimal(new int[] {
            3000,
            0,
            0,
            0});
            this.numericAutoTypeTimeout.Name = "numericAutoTypeTimeout";
            this.numericAutoTypeTimeout.Size = new System.Drawing.Size(63, 21);
            this.numericAutoTypeTimeout.TabIndex = 21;
            this.numericAutoTypeTimeout.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.numericAutoTypeTimeout.Value = new decimal(new int[] {
            3000,
            0,
            0,
            0});
            // 
            // VeraCryptOptionsForm
            // 
            this.AcceptButton = this.btnOk;
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.CancelButton = this.btnClose;
            this.ClientSize = new System.Drawing.Size(541, 292);
            this.Controls.Add(this.numericAutoTypeTimeout);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.llableHelp);
            this.Controls.Add(this.llblDonate);
            this.Controls.Add(this.btnRegistryResolve);
            this.Controls.Add(this.picBanner);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.lblSeparator);
            this.Controls.Add(this.btnOpenFileDialog);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtVeraCryptExecutable);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "VeraCryptOptionsForm";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "VeraCrypt Plugin Options";
            ((System.ComponentModel.ISupportInitialize)(this.picBanner)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericAutoTypeTimeout)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnOpenFileDialog;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtVeraCryptExecutable;
        private System.Windows.Forms.OpenFileDialog ofdVeraCryptExecutable;
        private System.Windows.Forms.Label lblSeparator;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox picBanner;
        private System.Windows.Forms.Button btnRegistryResolve;
        private System.Windows.Forms.ToolTip tipRegistryButton;
        private System.Windows.Forms.LinkLabel llblDonate;
        private System.Windows.Forms.LinkLabel llableHelp;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown numericAutoTypeTimeout;
    }
}