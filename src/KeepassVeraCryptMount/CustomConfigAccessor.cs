﻿/*
  Plugin for KeePass Password Safe - The Open-Source Password Manager
  KeePassVeraCryptMount
  Copyright (C) 2015-2017 Frédéric Bourqui

  fork of:
  KeePassTrueCryptMount
  Copyright (C) 2010-2013 Patrick Schaller

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

namespace KeePassVeraCryptMount
{   
    using KeePass.Plugins;
    
    internal static class CustomConfigAccessor
    {
        public static string GetVeraCryptExecutable(this IPluginHost host)
        {
            return host != null 
                ? host.CustomConfig.GetString(CustomConfigKeys.VeraCryptExecuteable) 
                : string.Empty;
        }

        public static void SetVeraCryptExecutable(this IPluginHost host, string filePath)
        {
            if (host != null)
            {
                host.CustomConfig.SetString(CustomConfigKeys.VeraCryptExecuteable, filePath);
            }
        }


        public static int GetVeraCryptAutoTypeWaitTimeout(this IPluginHost host)
        {
            return host != null 
                ? (int)host.CustomConfig.GetLong(CustomConfigKeys.VeraCryptAutoTypeWaitTimeout, 3000)
                : 3000;
        }

        public static void SetVeraCryptAutoTypeWaitTimeout(this IPluginHost host, int value)
        {
            if (host != null)
            {
                host.CustomConfig.SetLong(CustomConfigKeys.VeraCryptAutoTypeWaitTimeout, value);
            }
        }
    }
}
