﻿/*
  Plugin for KeePass Password Safe - The Open-Source Password Manager
  KeePassVeraCryptMount
  Copyright (C) 2015-2017 Frédéric Bourqui

  fork of:
  KeePassTrueCryptMount
  Copyright (C) 2010-2013 Patrick Schaller

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

namespace KeePassVeraCryptMount
{
    static class LanguageTexts
    {
        public const string MenuItemOpenDialogSuffix = "...";

        public const string BannerPluginTitleText = "VeraCrypt Mount Plugin";

        public const string BannerOptionsDescriptionText = "Setup global VeraCrypt options";

        public const string BannerMountFormDescriptionText = "Edit VeraCrypt mount entry settings";

        public const string TCMountMenuItemText = "Mount volume";

        public const string TCOptionsMenuItemText = "VeraCrypt Mount Plugin Options";
		
		public const string VCMountError = "VeraCrypt Mount Error";

        public const string VCNoVolumeSpecifiedError = "No volume has been specified.";

        public const string VCVolumeNotFound = "Volume not found: ";

        public const string VCPIMError = "PIM Error";

        public const string VCPIMOnlyNumber = "Please enter a positive integer";
    }
}
